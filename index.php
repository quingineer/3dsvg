<!DOCTYPE html>
<html>
    <head>
        <title>3DSVG</title>
        <link rel="shortcut icon" type="image/x-icon" href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAggSURBVHic3d3ri5xXHcDx7zOz92mztVkkWg3SSpFam4QtZNPUGEWDNaUieKF4aQVBBAuhBcG/Q3whiKCIonihaIk0WuzNVg20Wi+p1qrRaigpbZNNdjez55y+OM/P7E7n8lzO1S8cCLuBOc/zmTMzzzOzz4D/loETwI4At+WrRew2LMeeSNuWgXNAH/gDeaIsAn/EbsM5MkY5BJwHTDkuAX8FdsacVM12Yud8icvb8SqwEnNSTVrBTtwMjJxQhmFkiTIKYyvKX0gbZRxGViiTMHJAqYKRBUpVjJRR6mAkjVIXI0WUJhhJojTFSAmlDUZSKG0xBlGuDjt9wA1GEiiuMGKi7Cxv0wVGVBTXGDFQfGBsRdkfYBsAfxghUXxiBEXxjRECJQRGEJRQGD5RriYchleU0Bg+UGJgeEHZTxwMlygxMZyixMZwgZIChhOUVDDaoKSE0QolNYwmKCliNEJJFWMryrOMR0kZoxZK6hhVUHLAqISSC8Y4lJwwxqLkhiFjHThVQuSI8TqUovzHg+T5ER2ADeAFoANcA0zHnU7jzgFHCuAXwLvJd0O4wt7LWLV3sFzrA48WQA/4JXAjMBdzRk3aAfozgAHzLSjO25WSW+vAM8Bh+UEP+C2wRvzH08qjB+o+UKtgVsHcC5sLoGLPq+ZYK/f9wqBSVigLoI7B5stgto5jeaEIRm8QIyuUBVD3wOaLYIaNe/JAGYrRHQDpA98BPoR9GTk1TC1mC6A/C+bL0NXAsHErdFZBl5+QTvGJfh374fPDwIWtvxgEgYRR5kHfDeZLYzBkHNyCspkWykgMGA4CCaLMg/40mPugq7CPR5PGgRLlzySDMhYDRoNAQijzoD8J5liFlTE4VkqUU0RHmYgB40EgAZR50HfaJ+raGDL2Q+cC6GeJhlIJA6pPrgc8DLyTgAePc6A/AeaLk+84lfoKqO9DsR724LEyBtS7twjKjcBsk5nVaQ70x8B8wRGG9FVQPwyHUgsD6i/fIChzoD8C5vOOMaSvgfqxf5TaGNDs8dQryhzoD4P5nCcM6eug7veH0ggDmj/BeUGZBXOHPfDziiF9A9RPoNhwi9IYA9q94nCKMgvmKOi7AmFI3wT1gDuUdeyfUL+HBhjQ/iWgE5RZMLeB/lRgDOnboI5DZ6Pd/tjArozGGODmNXkrlFkwR+yxRhQM6bugHmyO4gQD3B0kNUKZAfMB0B+PjCF9D9TPoXOp3n5xhgFuj1p7wCPYg8eJKDNg3gf6o4lgSD8A9VB1FKcY4P40QiWUGTCH7bFGUhjSj0A9PBnFOQb4Oa8zFmUGzCHQdySKId0P6tHRKF4wwN+JtqEo02BuBX00cQzpAVCPQWfgTS5vGOD3zOcO4DHgemB2GswB0LdlgiEdB/XEZZQN7HHGITxggP9T0W8ATs7AtXtBHYVuCu8S1clgV8rT0L0EzwM3Ay/7ur0Q728YAA3FZoAb85G6fMfVvm/L5x12CXgCeCv2IUtfD9wOnVxWiQF+Crq8zEMH+5D1L+AAcNbHbfraN9sw5IclijmSyEPXuDkY4GclRn/7eS6vKD72y1AMaRr0dWDenwjKsAxwAtTzUPSHn3T0huJ6n4zFkKZAXwvmvQmiGOAhUH+3z3njzgB7QXG5PyphSFOg32YPEpNBMcAjoP4xGUNyjuJqX9TCkKZA7wZzMAEUAzwO6nR1DGkDOA3cggMUF/uhEYY0BfotYFYiohjgSVD/ro8hOUNpuw9aYUhToN8M5uYIKAY4Ceo/zTEkJyhttn8JeBLYjYO/vuqCfhOYfQFRDPAUqP9Cody8hdsapem2O8WQuqB3gXlXABQDPAPqjDsMqdUTfZPt9oIhdUG/EcwNHlEM8CdQL7rHkPrAP2mAUnebvWJIXdBLYN7h6czwKVBn/WFIfezD1wo1UOqABMGQOqB3gnm7Y5TnQL0EhQ7zUdLaKFVBrgIeB64jwOd6pU65UnY7QjldroxAGNIG8DfgIPDKpP9cBSToyhisA3rRAcppUK+Gx5Aqr5RJIFExpA7oK8Fc0xDlBVDn42FIlVDGgSSBIXVA98DsqolyBtSF+BjSRJRRIElhSAXoBTBLFVHOgroIhUkDQxKU/cBLg78cBpIkhlSAngdz1QSUV0CtpYchyXHKCgMogyBJY0gF6FkwV45AOQ9qI10MaSjKVpAsMKQC9LS9qsM2lIug+uljSCNXyhLwHPld/EtNwWYPTA/MFGyS/iU1Bse2L0oryn/8mkxWxpB0p9y48pVUDitjsP+tlAJ7raxbyBPj/6k+8KsCe0h/HLiCNC4/UbcLwBnsytgFzMedTqMMsAp8UH6wD3ueRRP/MbXOWAN+h/161EXg9yR+aakhQ5f7ft+gUm4oWzGk3FBGYuSGMgxDWix/lzrKRAwpdRTBGHdZ29RRKmNIqaJUwZBSRRGMvRW2YVupodTBkHaQFkpjDGkvaaCsAU/T7OrbqaC0xpBio6wBJxlzKdUKxUZxhiHFQpl4XdsaxUJxjiGFRnGJIfWwqy0UijcMKRSKDwwpFIp3DMk3ik8MyTdKMAzJF0oIDMkXimDsCbAN23KNEhJDco0SDUNyhRIDQ3L1BQTRMaQ9tEOJiSG1RUkGQ2qKkgKG1BRFYy+1kQyGVBclJQypLkqyGNIe7AQnoawBvyEtDElQLpI5hjQJRTBe951LCTUJRTBuijXBuo1CyQFD6mHnOoiSHYZ0E9tRcsKQFrBzlueUbDEkudjXOqPfA089+eDEOnZbluNOp33LwAny/WpXsCgnCIDxGn+KgKxLnMVcAAAAAElFTkSuQmCC" />
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
        <style type="text/css">
            html,body {
                height: 100%;
            }
            body {
                margin: 0;
                position: relative;
                cursor: default;
                overflow: hidden;
            }
            ::selection {
                background: rgba(0,0,0,0);
            }
            
            .section {
                position: absolute;
            }
            
            .img {
                position: absolute;
                top: 50%;
                left: 50%;
                margin-left: -20px;
                margin-top: -21px;
                width: 100%;
                height: 100%;
                display: block;
                font-size: 1em;
                z-index: 1;
            }
            .view > .img,
            .float > .img {
                display: none;
            }
            
            .button {
                position: relative;
                overflow: hidden;
                color: #000;
                text-shadow: 0 1px 0 #fff;
                font-weight: bold;
                display: inline-block;
                width: 30px;
                height: 30px;
                font-size: 30px;
                vertical-align: middle;
                font-family: sans-serif;
                padding: 5px;
                border: 2px outset #666;
                box-shadow: -5px -5px 10px rgba(0,0,0,0.5) inset;
                border-radius: 30px;
                background-color: #fff;
                text-decoration: none;
                margin: 8px;
                cursor: pointer;
            }
            .float,.view {
                background-color: #7fff00;
            }
            .button[selected] {
                background-color: #7f00ff;
                color: #fff;
                text-shadow: 0 -1px 0 #000;
                font-weight: normal;
            }
            .button:active {
                color: #7f7f7f;
                text-shadow: 0 1px 1px #000;
                background-color: #3f007f;
                border-style: inset;
            }
            .view:active,
            .float:active {
                background-color: #3f7f00;
            }
            
            .wrap {
                position: relative;
                font-size: 0;
                height: 100%;
                display: table-cell;
            }
            
            .tab {
                position: relative;
                z-index: 1;
                white-space: nowrap;
                color: #fff;
                padding: 8px;
                text-shadow: 0 -1px 0 #000;
                text-decoration: none;
                font-size: 9pt;
                font-weight: bold;
                font-family: serif;
                text-align: center;
                letter-spacing: 1px;
                cursor: pointer;
                text-transform: capitalize;
            }
            .tab[selected] {
                background-color: #fff;
                color: #7f00ff;
                z-index: 3;
                text-shadow: 0 -1px 0 #000;
                cursor: default;
            }
            
            #screen {
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                z-index: 1;
                display: table;
                background-color: #fff;
                position: relative;
            }
            body[pos="r"] > #screen {
                left: -42px;
            }
            body[pos="l"] > #screen {
                left: 42px;
            }
            body[pos="l"] > #screen,
            body[pos="r"] > #screen {
                border-spacing: 59px 17px;
            }
            body[pos="b"] > #screen {
                top: -24px;
            }
            body[pos="t"] > #screen {
                top: 24px;
            }
            body[pos="t"] > #screen,
            body[pos="b"] > #screen {
                border-spacing: 17px 41px;
            }
            
            #source {
                position: absolute;
                resize: none;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                border: none;
                padding: 8px;
                margin: -9px 0 0 -9px;
                outline: none;
                font-size: 11pt;
                white-space: nowrap;
                overflow: auto;
                border: 1px dashed #7f7f7f;
            }
            #source::selection {
                background: #7f00ff;
                color: #fff;
            }
            
            #canvas {
                display: block;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                background-color: #fff;
                border: 1px dashed #7f7f7f;
                padding: 8px;
                margin: -9px 0 0 -9px;
            }
            #canvas > [name="point"] {
                fill: #f00;
                /*opacity: 0;*/
            }
            #canvas > [name="point"].link {
                display: none;
            }
            #canvas > [name="point"].anchor {
                fill: #7f00ff;
            }
            #canvas > [name="point"].curve {
                fill: #fff;
                stroke-width: 2;
                stroke: #000;
            }
            #canvas > [name="point"]:hover {
                opacity: 0.5;
            }
            #canvas > [name="point"][selected] {
                opacity: 1;
            }
            body[tool="0"] #canvas {
                cursor: default;
            }
            body[tool="1"] #canvas {
                cursor: crosshair;
            }
            
            #nav {
                background-color: #7f7f7f;
                overflow: hidden;
                z-index: 3;
            }
            #nav::before {
                position: absolute;
                z-index: 2;
                content: '';
                display: block;
            }
            #nav > .tab:not([selected]):active {
                background-color: #7f00ff;
            }
            body[pos="l"] > #nav,
            body[pos="r"] > #nav {
                top: 0;
                height: 100%;
                width: 84px;
                overflow-y: scroll;
            }
            body[pos="t"] > #nav,
            body[pos="b"] > #nav {
                left: 0;
                height: 48px;
                width: 100%;
                overflow-x: scroll;
            }
            body[pos="r"] > #nav {
                right: 0;
            }
            body[pos="r"] > #nav::before {
                border-left: 1px solid #000;
                left: 0;
                top: 0;
                height: 100%;
            }
            body[pos="l"] > #nav {
                left: 0;
            }
            body[pos="l"] > #nav::before {
                border-right: 1px solid #000;
                right: 0;
                top: 0;
                height: 100%;
            }
            body[pos="t"] > #nav {
                top: 0;
            }
            body[pos="t"] > #nav::before {
                border-bottom: 1px solid #000;
                bottom: 0;
                left: 0;
                width: 100%;
            }
            body[pos="b"] > #nav {
                bottom: 0;
            }
            body[pos="b"] > #nav::before {
                border-top: 1px solid #000;
                top: 0;
                left: 0;
                width: 100%;
            }
            body[pos="l"] > #nav > .tab,
            body[pos="r"] > #nav > .tab {
                display: block;
                border-bottom: 1px solid #000;
            }
            body[pos="t"] > #nav > .tab,
            body[pos="b"] > #nav > .tab {
                display: table-cell;
                border-right: 1px solid #000;
            }
        </style>
        <script type="text/javascript">
var global = {};

/**
 * Get elements with a certain attribute.
 * Can also filter attributes by their values.
 * 
 * @param {array} args [attribute_name,attribute_value (optional)]
 */
global.getElementsByAttribute = function ()
{
  var attribute = arguments[0];
  var value = arguments[1] || false;
  var elements = document.body.children;
  var found = [];
  
  for (var i = 0; i < elements.length; i++)
  {
    var ele = elements[i];
    var attributeLen = ele.attributes.length;
    for (var a = 0; a < attributeLen; a++)
    {
      if (ele.attributes[a].name.toLowerCase() === attribute)
      {
        if (value && ele.getAttribute(attribute) === value)
        {
          found.push(ele);
        }
        else if (!value)
        {
          found.push(ele);
        }
        break;
      }
    }
  }

  return found;
};

/**
 * Get element by it's "name" attribute value and html object value.
 * 
 * @param {string} name
 * @param {mixed} value
 * @returns {array} DocumentOject
 */
global.getUniqueElement = function(name, value)
{
  var found = global.getElementsByAttribute('name',name);
  var elements = (found.length > 0) ? found : false;
  
  if (elements)
  {
    for (var i = 0; i < elements.length; i++)
    {
      var ele = elements[i];
      if (ele.value === value)
      {
        return ele;
      }
    }
  }
  
  return false;
};

/**
 * Append script to page.
 * 
 * @param {string} src - Script source string.
 */
global.appendScript = function(src)
{
    var script = document.createElement('script');
    script.src = src;
    document.body.insertBefore(script,null);

    return true;
};

/**
 * Check if element has any or all of specified attributes.
 * 
 * @param {DocumentObject} element
 * @param {array} attributes
 * @param {string} mode ('any'|'all')
 */
global.hasAttributes = function(element,attributes,mode)
{
    for (var i=0; i<attributes.length; i++)
    {
        if (mode === 'any' && element.hasAttribute(attributes[i]))
        {
            return true;
        }
        else if (mode === 'all' && !element.hasAttribute(attributes[i]))
        {
            return false;
        }
    }
    
    if (mode === 'any')
    {
        return false;
    }
    else if (mode === 'all')
    {
        return true;
    }
};

/**
 * Remove non-element nodes from an elements nodes.
 * 
 * @param {DocumentObject} parent
 */
global.filterNodes = function(parent)
{
    var childNodes = parent.childNodes;
    
    for (var i=0; i<childNodes.length; i++)
    {
        var child = childNodes[i];
        
        if (child.nodeName.indexOf('#'))
        {
            child.nodeValue = '';
        }
    }
};

/**
 * Return a random number from 0 to specified real number range.
 * Higer seed value - lower repetitive outputs.
 * 
 * @param {number} range
 * @param {number} seed
 * @returns {number}
 */
global.pseudoRand = function(range,seed)
{
    var repeat = seed ? seed : range*range;
    var vals = [];
    
    for (var i=0; i<repeat; i++)
    {
        for (var j=0; j<range; j++)
        {
            vals.push(j);
        }
    }
    
    var index = Math.round(Math.random()*vals.length);
    
    return vals[index];
};

/**
 * Self explanatory.
 * 
 * @returns {Number}
 */
global.timestamp = function()
{
    if (!Date.now)
    {
        return new Date().getTime();
    }
    
    return Date.now();
};

/**
 * Generates a unique id from the current timestamp.
 * Unique id gets saved to memory array global.storedID.
 * 
 * @returns {String}
 */
global.uID = function()
{
    var id = global.timestamp() + '';
    
    while (global.storedID.indexOf(id) !== -1)
    {
        id = global.timestamp() + '';
    }
    
    global.storedID.push(id);
    
    return id;
};

/**
 * Get element by it's id and remove it.
 * 
 * @param {string} id
 */
global.removeElementsById = function(ids)
{
    for (var i=0; i<ids.length; i++)
    {
        var element = document.getElementById(ids[i]);

        if (element !== null && element !== undefined)
        {
            element.parentNode.removeChild(element);
        }
    }
};

/**
 * Adds or removes an event listener to a specified object.
 * 
 * @param {object} object
 * @param {string} method (add|remove) Add or remove event listener.
 * @param {string} type - event type load,resize,click,etc.
 * @param {function} handle - function to execute
 */
global.event = function(object,method,type,handle)
{
    if (object !== undefined)
    {
        if (method === 'add')
        {
            return object.addEventListener(type,handle);
        }
        else if (method === 'remove')
        {
            return object.removeEventListener(type,handle);
        }
    }
    
    return false;
};

global.sum = function(array)
{
    return array.reduce(function(previousReturnValue,currentIndexValue){
        return previousReturnValue + currentIndexValue;
    });
};

global.flatten = function(array)
{
    return array.reduce(function(previousReturnValue,currentIndexValue){
        return previousReturnValue.concat(currentIndexValue);
    });
};

global.getStyle = function(element,property)
{
    if (element.currentStyle)
    {
        return element.currentStyle(property);
    }
    else if (window.getComputedStyle)
    {
        return document.defaultView.getComputedStyle(element,null).getPropertyValue(property);
    }
};

global.fileExists = function(path)
{
    path = path.substr(path.indexOf(window.location.hostname)+window.location.hostname.length,path.length);
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest : new ActiveXObject("MICROSOFT.XMLHTTP");
    xhr.open('GET',path,false);
    xhr.send();
    return (xhr.status === 200) ? true : false;
};

global.ajax = function()
{
    var args = arguments;
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("MICROSOFT.XMLHTTP");
    var sendType = args[0];
    var path = args[1];
    var returnType = args[2];
    var data = args[3];
    var handle = args[4];
    
    xhr.open(sendType,path,true);
    xhr.onreadystatechange = function()
    {
        if (xhr.status === 200 && xhr.readyState === 4)
        {
            var response;
            
            switch (returnType)
            {
                case 'xml': response = xhr.responseXML;
                    break;
                case 'text': response = xhr.responseText;
                    break;
                case 'json': response = JSON.parse(xhr.responseText);
                    break;
                case 'fdat': response = global.jsonFormData(JSON.parse(xhr.responseText));
                    break;
            }
            
            handle(response);
        }
    };
    xhr.send(data);
};

global.parseXML = function(string)
{
    var parse;
    
    if (window.DOMParser)
    {
        parse = function(xmlstr)
        {
            return (new window.DOMParser()).parseFromString(xmlstr,'text/xml');
        };
    }
    else if (typeof window.ActiveXObject != 'undefined'
                && new window.ActiveXObject('Microsoft.XMLDOM'))
    {
        parse = function(xmlstr)
        {
            var xmldoc = new window.ActiveXObject('Microsof.XMLDOM');
            xmldoc.async = 'false';
            xmldoc.loadXML(xmlstr);
            return xmldoc;
        };
    }
    else
    {
        parse = function(){return null;};
    }
    
    return parse(string).documentElement;
};

global.jsonFormData = function(jsonData)
{
    var formData = new FormData();
    var props = Object.keys(jsonData);
    
    for (var i=0; i<props.length; i++)
    {
        var name = props[i];
        var data = jsonData[name];
        var names = Object.keys(data);
        var n = 0;
        
        while (n < names.length)
        {
            name += '['+names[n]+']';
            data = data[names[n]];

            if (typeof data === 'object')
            {
                names = Object.keys(data);
            }
            else
            {
                formData.append(name,data);
                name = props[i];
                data = jsonData[name];
                names = Object.keys(data);
                n++;
            }
        }
    }
    
    return formData;
};
        </script>
    </head>
    <body pos="l" tool="1" view="0" style="opacity: 0.01;">
        <div id="screen" class="section">
            <div class="wrap">
                <textarea id="source" placeholder="Source Code" autofocus="">[ {"label":"layer-1", "groups":
   [ {"label":"group-1", "objects":
      [ {"label":"object-1", "lines":
         [ {"label":"line-1", "points":
            [  ]
         } ]
      } ]
   } ]
} ]</textarea>
<!--                <textarea id="source" placeholder="Source Code" autofocus="">[ {"label":"layer-1", "groups":
   [ {"label":"group-1", "objects":
      [ {"label":"object-1", "lines":
         [ {"label":"line-1", "points":
            [ {"x":-100, "y":-100, "z":0, "links":[]},
              {"x":100, "y":-100, "z":0, "links":[]} ]
         },{"label":"line-2", "points":
            [ {"x":100, "y":-100, "z":0, "links":[]},
              {"x":100, "y":100, "z":0, "links":[]} ]
         },{"label":"line-3", "points":
            [ {"x":100, "y":100, "z":0, "links":[]},
              {"x":-100, "y":100, "z":0, "links":[]} ]
         },{"label":"line-4", "points":
            [ {"x":-100, "y":100, "z":0, "links":[]},
              {"x":-100, "y":-100, "z":0, "links":[]} ]
         },{"label":"line-5", "points":
            [ {"x":-100, "y":-100, "z":0, "links":[]},
              {"x":-100, "y":-100, "z":200, "links":[]} ]
         },{"label":"line-6", "points":
            [ {"x":-100, "y":-100, "z":200, "links":[]},
              {"x":100, "y":-100, "z":200, "links":[]} ]
         },{"label":"line-7", "points":
            [ {"x":100, "y":-100, "z":200, "links":[]},
              {"x":100, "y":-100, "z":0, "links":[]} ]
         },{"label":"line-8", "points":
            [ {"x":100, "y":100, "z":0, "links":[]},
              {"x":100, "y":100, "z":200, "links":[]} ]
         },{"label":"line-9", "points":
            [ {"x":100, "y":100, "z":200, "links":[]},
              {"x":100, "y":-100, "z":200, "links":[]} ]
         },{"label":"line-10", "points":
            [ {"x":100, "y":100, "z":200, "links":[]},
              {"x":-100, "y":100, "z":200, "links":[]} ]
         },{"label":"line-3", "points":
            [ {"x":-100, "y":100, "z":200, "links":[]},
              {"x":-100, "y":100, "z":0, "links":[]} ]
         },{"label":"line-3", "points":
            [ {"x":-100, "y":100, "z":200, "links":[]},
              {"x":-100, "y":-100, "z":200, "links":[]} ]
         } ]
      } ]
   } ]
} ]</textarea>-->
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="canvas" style="display: none;" class="section"></svg>
            </div>
        </div>
        <div id="nav" class="section">
            <a href="javascript: void(0);" target="_self" class="tab" id="code" selected="">&#91; &#123; &#8943; &#125; &#93;</a>
            <a href="javascript: void(0);" target="_self" class="tab" id="window">Window</a>
        </div>
        <script type="text/javascript">
var SVG_NS = 'http://www.w3.org/2000/svg';
var CELL_SIZE = 30;
var STATE = '';
var ORIGIN = {z:0};
var LOCAL = {};
var CURRENT = [0,0,0,0];
var VIEW = 0;

var Canvas = new function()
{
    this.element = document.getElementById('canvas');
    this.layers = [];
    this.tool = true;
    this.active = false;
    
    this.events = function()
    {
        var e = this.element;
        
        global.event(e,'add','mousedown',this.handle_pointerdown);
        global.event(e,'add','touchmove',this.handle_pointermove);
        global.event(e,'add','mousemove',this.handle_pointermove);
        global.event(e,'add','mouseup',this.handle_pointerup);
        global.event(e,'add','click',this.handle_pointerclick);
        global.event(e,'add','dblclick',this.handle_pointerdblclick);
        global.event(e,'add','contextmenu',this.handle_pointermenu);
        global.event(e,'add','mousewheel',this.handle_pointerwheel);
        global.event(window,'add','resize',this.handle_resize);
        global.event(window,'add','load',this.handle_load);
    };
    
    this.handle_pointermenu = function(event)
    {
        event.preventDefault();
        Canvas.toggle_tool();
        STATE = '';
    };
    
    this.handle_pointerclick = function(event)
    {
        if (STATE === 'PLOT')
        {
            Canvas.plot(LOCAL.x-ORIGIN.x,LOCAL.y-ORIGIN.y,0);
        }
        
        STATE = '';
    };
    
    this.handle_pointerdblclick = function(event)
    {
        
    };
    
    this.handle_pointerwheel = function(event)
    {
        
    };
    
    this.handle_load = function(event)
    {
        Canvas.set_origin();
    };
    
    this.handle_resize = function(event)
    {
        Canvas.set_origin();
    };
    
    this.handle_pointerup = function(event)
    {
        if (STATE === 'DRAW')
        {
            Canvas.layers[CURRENT[0]].groups[CURRENT[1]].objects[CURRENT[2]].lines[CURRENT[3]++].curve();
            STATE = '';
        }
    };
    
    this.handle_pointermove = function(event)
    {
        event.preventDefault();
        
        if (STATE === 'PLOT' || STATE === 'DRAW')
        {
            var rect = Canvas.element.getBoundingClientRect();
            var e_type = event.type;
            var paddingLeft = parseInt(global.getStyle(Canvas.element,'padding-left'));
            var paddingTop = parseInt(global.getStyle(Canvas.element,'padding-top'));
            var x = e_type === 'mousemove' ? event.clientX - rect.left - paddingLeft : event.touches[0].clientX - rect.left - paddingLeft;
            var y = e_type === 'mousemove' ? event.clientY - rect.top - paddingTop : event.touches[0].clientY - rect.top - paddingTop;
            var dx = x - LOCAL.x;
            var dy = y - LOCAL.y;
            var h = Math.sqrt(Math.pow(dx,2) + Math.pow(dy,2));

            if (h >= CELL_SIZE)
            {
                if (STATE === 'PLOT')
                {
                    CURRENT[3] = Canvas.layers[CURRENT[0]].groups[CURRENT[1]].objects[CURRENT[2]].lines.length;
                    Canvas.plot(LOCAL.x-ORIGIN.x,LOCAL.y-ORIGIN.y,0);
                    STATE = 'DRAW';
                }

                if (STATE === 'DRAW')
                {
                    LOCAL.x = x;
                    LOCAL.y = y;
                    Canvas.draw(x-ORIGIN.x,y-ORIGIN.y,0);
                }
            }
        }
    };
    
    this.handle_pointerdown = function(event)
    {
        if (Canvas.tool && STATE === '')
        {
            if (event.which === 1 || event.touches !== undefined && event.touches.length === 1)
            {
                var rect = Canvas.element.getBoundingClientRect();
                var e_type = event.type;
                var paddingLeft = parseInt(global.getStyle(Canvas.element,'padding-left'));
                var paddingTop = parseInt(global.getStyle(Canvas.element,'padding-top'));
                var x = e_type === 'mousedown' ? event.clientX - rect.left - paddingLeft : event.touches[0].clientX - rect.left - paddingLeft;
                var y = e_type === 'mousedown' ? event.clientY - rect.top - paddingTop : event.touches[0].clientY - rect.top - paddingTop;
                
                LOCAL.x = x;
                LOCAL.y = y;
                STATE = 'PLOT';
            }
        }
    };
    
    this.plot = function(x,y,z)
    {
        var li = CURRENT[0];
        var gi = CURRENT[1];
        var oi = CURRENT[2];
        var ci = CURRENT[3];
        var layers = this.layers;
        var layer = (li === layers.length) ? layers[li] = new Layer() : layers[li];
        var group = (gi === layer.groups.length) ? layer.groups[gi] = new Group(layer) : layer.groups[gi];
        var object = (oi === group.objects.length) ? group.objects[oi] = new Object(group) : group.objects[oi];
        var line = (ci === object.lines.length) ? object.lines[ci] = new Line(object) : object.lines[ci];
        var point = new Point(x,y,z);
        
        point.data = [li,gi,oi,ci];
        line.plot(point);
    };
    
    this.draw = function(x,y,z)
    {
        var li = CURRENT[0];
        var gi = CURRENT[1];
        var oi = CURRENT[2];
        var ci = CURRENT[3];
        var line = this.layers[li].groups[gi].objects[oi].lines[ci];
        var point = new Point(x,y,z);
        
        point.data = [li,gi,oi,ci];
        line.draw(point);
    };
    
    this.set_origin = function()
    {
        var canvas = this.element;
        var element = this.active ? canvas : Source.element;
        var rect = element.getBoundingClientRect();
        var w = rect.width;
        var h = rect.height;
        var pos = Nav.element.getAttribute('pos');
        var offsetX = 9;
        var offsetY = 9;
        
        if ((['l','r']).indexOf(pos) !== -1)
        {
            offsetX += 85;
        }
        else
        {
            offsetY += 48;
        }
        
        ORIGIN.x = (w/2) - offsetX;
        ORIGIN.y = (h/2) - offsetY;
        LOCAL.width = w;
        LOCAL.height = h;
        
        this.update();
    };
    
    this.update = function()
    {
        var layers = this.layers;
        
        for (var i=0; i<layers.length; i++)
        {
            layers[i].update();
        }
    };
    
    this.toggle_tool = function()
    {
        var active = this.tool = !this.tool;
        
        document.body.setAttribute('tool',+active);
    };
    
    this.delete = function()
    {
        var children = this.element.children;
        
        for (var i=0; i<children.length;)
        {
            var child = children[i];
            
            child.parentNode.removeChild(child);
        }
        
        this.layers.splice(0);
        
        return this.layers;
    };
    
    this.load_data = function()
    {
        var json = Source.get_data();
        var data,layers = this.delete();
        
        for (var i=0; i<json.length; i++)
        {
            data = json[i];
            
            var layer = new Layer();
            var groups = data.groups;
            
            layer.label = data.label;
            
            for (var j=0; j<groups.length; j++)
            {
                data = groups[j];
                
                var group = new Group(layer);
                var objects = data.objects;
                
                group.label = data.label;
                
                for (var k=0; k<objects.length; k++)
                {
                    data = objects[k];
                    
                    var object = new Object(group);
                    var lines = data.lines;
                    
                    object.label = data.label;
                    
                    for (var l=0; l<lines.length; l++)
                    {
                        data = lines[l];
                        
                        var line = new Line(object);
                        var points = data.points;
                        var plen = points.length;
                        
                        for (var m=0; m<plen; m++)
                        {
                            data = points[m];
                            
                            var x = parseInt(data.x);
                            var y = parseInt(data.y);
                            var z = parseInt(data.z);
                            var point = new Point(x,y,z);
                            
                            point.type = (m === 0 || m === plen-1) ? '' : 'curve';
                            point.data = data.links;
                            line.points.push(point);
                        }
                    }
                }
            }
        }
        
        for (var i=0; i<layers.length; i++)
        {
            var groups = layers[i].groups;
            
            for (var j=0; j<groups.length; j++)
            {
                var objects = groups[j].objects;
                
                for (var k=0; k<objects.length; k++)
                {
                    var lines = objects[k].lines;
                    
                    for (var l=0; l<lines.length; l++)
                    {
                        var line = lines[l];
                        var points = line.points;
                        
                        for (var m=0; m<points.length; m++)
                        {
                            var point = points[m];
                            var links = point.data;
                            
                            for (var n=0; n<links.length; n++)
                            {
                                data = links[n];
                                point.link(layers[data[0]].groups[data[1]].objects[data[2]].lines[data[3]].points[data[4]]);
                            }
                            
                            point.data = [i,j,k,l,m];
                            point.line = line;
                            point.update();
                        }
                    }
                }
            }
        }
    };
    
    this.init = function()
    {
        this.events();
        this.set_origin();
    };
};

var Nav = new function()
{
    this.element = document.getElementById('nav');
    this.tabs = [];
    this.selected;
    this.pos = {h:'l',v:'t'};
    this.size = {h:48,v:84};
    
    this.get_tabs = function()
    {
        this.tabs.splice();
        
        var tabs = document.getElementById('nav').children;
        
        for (var i=0; i<tabs.length; i++)
        {
            var tab = tabs[i];
            
            this.tabs.push(tab);
            
            if (tab.hasAttribute('selected')) {this.selected = tab;}
        }
    };
    
    this.events = function()
    {
        global.event(window,'add','resize',this.handle_resize);
        global.event(window,'add','load',this.handle_resize);
        this.tab_events();
    };
    
    this.tab_events = function()
    {
        var tabs = this.tabs;
        
        for (var i=0; i<tabs.length; i++)
        {
            (function(t)
            {
                global.event(t,'add','click',Nav.handle_pointerclick);
            })(tabs[i]);
        }
    };
    
    this.handle_resize = function(event)
    {
        var pos = Nav.pos;
        var body = document.body;
        var element = Source.active ? Source.element : Canvas.element;
        var rect = element.getBoundingClientRect();
        
        if (rect.width < rect.height - Nav.size.v)
        {
            body.setAttribute('pos',pos.v);
        }
        else if (rect.height < rect.width - Nav.size.h)
        {
            body.setAttribute('pos',pos.h);
        }
        
        body.style.opacity = 1;
    };
    
    this.handle_pointerclick = function(event)
    {
        var tab_index = Nav.tabs.indexOf(event.currentTarget);
        var tab = Nav.tabs[tab_index];

        if (tab_index)
        {
            Canvas.load_data();
            Source.active = false;
            Canvas.active = true;
            Canvas.element.style.display = '';
            Source.element.style.display = 'none';
        }
        else
        {
            Source.save_data();
            Source.active = true;
            Canvas.active = false;
            Source.element.style.display = '';
            Canvas.element.style.display = 'none';
        }
        
        Nav.selected.removeAttribute('selected');
        Nav.selected = tab;
        tab.setAttribute('selected','');
    };
    
    this.init = function()
    {
        this.get_tabs();
        this.events();
    };
};

var Source = new function()
{
    this.element = document.getElementById('source');
    this.active = true;
    this.data;
    
    this.get_data = function()
    {
        this.data = JSON.parse(this.element.value);
        
        return this.data;
    };
    
    this.save_data = function()
    {
        var data = '[ ';
        var layers = Canvas.layers;
        
        for (var i=0; i<layers.length; i++)
        {
            var layer = layers[i];
            var groups = layer.groups;
            
            data += '{"label":"'+layer.label+'","groups":\n   [ ';
            
            for (var j=0; j<groups.length; j++)
            {
                var group = groups[j];
                var objects = group.objects;
                
                data += '{"label":"'+group.label+'","objects":\n      [ ';
                
                for (var k=0; k<objects.length; k++)
                {
                    var object = objects[k];
                    var lines = object.lines;
                    
                    data += '{"label":"'+object.label+'","lines":\n         [ ';
                    
                    for (var l=0; l<lines.length; l++)
                    {
                        var line = lines[l];
                        var points = line.points;
                        
                        data += '{"label":"'+line.label+'","points":\n            [ ';
                        
                        for (var m=0; m<points.length; m++)
                        {
                            var point = points[m];
                            var links = point.links;
                            
                            data += '{"x":'+point.x+',"y":'+point.y+',"z":'+point.z+',';
                            data += '"links":\n               [ ';
                            
                            for (var n=0; n<links.length; n++)
                            {
                                data += '['+links[n].data.join()+'],';
                            }
                            
                            data = data.slice(0,-1);
                            data += ' ]\n            },';
                        }
                        
                        data = data.slice(0,-1);
                        data += ' ]\n         },';
                    }
                    
                    data = data.slice(0,-1);
                    data += ' ]\n      },';
                }
                
                data = data.slice(0,-1);
                data += ' ]\n   },';
            }
            
            data = data.slice(0,-1);
            data += ' ]\n},';
        }
        
        data = data.slice(0,-1);
        data += ' ]';
        Source.element.innerHTML = data;
    };
};

var Color = function(r,g,b,a)
{
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
    this.bmd;
    
    this.string = function()
    {
        return 'rgba('+this.r+','+this.g+','+this.b+','+this.a+')';
    };
};

var Layer = function()
{
    Canvas.layers.push(this);
    this.label = 'layer-'+Canvas.layers.length;
    this.groups = [];
    
    this.delete = function()
    {
        var index = Canvas.layers.indexOf(this);
        
        Canvas.layers.splice(index,1);
        CURRENT[0] = Canvas.layers.length;
        CURRENT[1] = 0;
    };
    
    this.update = function()
    {
        var groups = this.groups;

        for (var i=0; i<groups.length; i++)
        {
            groups[i].update();
        }
    };
    
    this.translate = function(dx,dy,dz)
    {
        var groups = this.groups;

        for (var i=0; i<groups.length; i++)
        {
            groups[i].translate(dx,dy,dz);
        }
    };
};

var Group = function(layer)
{
    layer.groups.push(this);
    this.label = 'group-'+layer.groups.length;
    this.layer = layer;
    this.objects = [];
    
    this.delete = function()
    {
        var layer = this.layer;
        var index = layer.groups.indexOf(this);
        
        layer.groups.splice(index,1);
        CURRENT[1] = layer.groups.length;
        CURRENT[2] = 0;
        
        if (!layer.grouops.length) {layer.delete();}
    };
        
    this.update = function()
    {
        var objects = this.objects;

        for (var i=0; i<objects.length; i++)
        {
            objects[i].update();
        }
    };
    
    this.translate = function(dx,dy,dz)
    {
        var objects = this.objects;

        for (var i=0; i<objects.length; i++)
        {
            objects[i].translate(dx,dy,dz);
        }
    };
};

var Object = function(group)
{
    group.objects.push(this);
    this.label = 'object-'+group.objects.length;
    this.group = group;
    this.lines = [];
    this.color = new Color(255,255,255,1);
    
    this.delete = function()
    {
        var group = this.group;
        var index = group.objects.indexOf(this);
        
        group.objects.splice(index,1);
        CURRENT[2] = group.objects.length;
        CURRENT[3] = 0;
        
        if (!group.objects.length) {group.delete();}
    };
    
    this.update = function()
    {
        var lines = this.lines;

        for (var i=0; i<lines.length; i++)
        {
            var points = lines[i].points;
            
            for (var j=0; j<points.length; j++)
            {
                var point = points[j];
                
                if (point.anchor === null)
                {
                    point.update();
                }
            }
        }
    };
    
    this.translate = function(dx,dy,dz)
    {
        var lines = this.lines;

        for (var i=0; i<lines.length; i++)
        {
            lines[i].translate(dx,dy,dz);
        }
    };
};

var Line = function(object)
{
    object.lines.push(this);
    this.label = 'line-'+object.lines.length;
    this.object = object;
    this.points = [];
    this.element = document.createElementNS(SVG_NS,'path');
    
    this.init = function()
    {
        var element = this.element;
        var canvas = Canvas.element;
        
        element.setAttribute('d','');
        element.setAttribute('stroke','#000');
        element.setAttribute('stroke-width',5);
        element.setAttribute('stroke-linejoin','round');
        element.setAttribute('stroke-linecap','round');
        element.setAttribute('fill','none');
        element.setAttribute('name','line');
        canvas.insertBefore(element,canvas.children[0]);
    };
    
    this.plot = function(p)
    {
        var l = this;
        var plen = this.points.length;
        
        if (STATE === 'PLOT' && plen > 1)
        {
            var o = l.object;
            var end = l.points[plen-1];
            var c = end.clone();
            
            l = new Line(o);
            l.points.push(c);
            c.line = l;
            p.data[3] = c.data[3] = ++CURRENT[3];
            end.link(c);
        }
        
        p.line = l;
        p.element.setAttribute('data',p.data.join());
        l.points.push(p);
        p.update();
    };
    
    this.draw = function(p)
    {
        var element = this.element;
        var d = element.getAttribute('d')+' L'+p.cx+' '+p.cy;
        
        this.points.push(p);
        p.type = 'link';
        p.update();
        p.line = this;
        element.setAttribute('d',d);
    };
    
    this.curve = function()
    {
        var points = this.points;
        var plen = points.length;
        var pdir,indices = [];
        var count=0;
        
        //Remove same slope lines and zig-zags,
        //Plot points that begin a change in direction or a continuous line.
        for (var i=1; i<plen-1 && plen > 2; i++)
        {
            var p1 = points[i-1];
            var p2 = points[i];
            var p3 = points[i+1];
            var x1 = p1.x;
            var x2 = p2.x;
            var x3 = p3.x;
            var y1 = p1.y;
            var y2 = p2.y;
            var y3 = p3.y;
            var dx1 = x2 - x1;
            var dy1 = y2 - y1;
            var dx2 = x3 - x2;
            var dy2 = y3 - y2;
            var m1 = dy1 / dx1;
            var m2 = dy2 / dx2;
            
            //Straight lines and overlaps.
            if (m1 === m2 || m1 / m1 === NaN)
            {
                dir = 0;
            }
            else
            {
                var dir = (
                dx1 >= 0 && dy1 >= 0 && dx2 < 0 && dy2 < 0 && m1 > m2//  \3 c
/*
                || dx1 >= 0 && dy1 >= 0 && dx2 < 0 && dy2 < 0 && m1 < m2//  \3 cc
                || dx1 >= 0 && dy1 >= 0 && dx2 >= 0 && dy2 <= 0//           \4 cc
                || dx1 >= 0 && dy1 >= 0 && dx2 > 0 && dy2 > 0 && m1 > m2//  \1 cc
*/
                || dx1 >= 0 && dy1 >= 0 && dx2 > 0 && dy2 > 0 && m1 < m2//  \1 c
                || dx1 >= 0 && dy1 >= 0 && dx2 <= 0 && dy2 >= 0//           \2 c

                || dx1 <= 0 && dy1 >= 0 && dx2 > 0 && dy2 < 0 && m1 > m2//  /4 c
/*
                || dx1 <= 0 && dy1 >= 0 && dx2 > 0 && dy2 < 0 && m1 < m2//  /4 cc
                || dx1 <= 0 && dy1 >= 0 && dx2 >= 0 && dy2 >= 0//           /1 cc
                || dx1 <= 0 && dy1 >= 0 && dx2 < 0 && dy2 > 0 && m1 > m2//  /2 cc
*/
                || dx1 <= 0 && dy1 >= 0 && dx2 < 0 && dy2 > 0 && m1 < m2//  /2 c
                || dx1 <= 0 && dy1 >= 0 && dx2 <= 0 && dy2 <= 0//           /3 c

                || dx1 <= 0 && dy1 <= 0 && dx2 > 0 && dy2 > 0 && m1 > m2//  \1 c
/*
                || dx1 <= 0 && dy1 <= 0 && dx2 > 0 && dy2 > 0 && m1 < m2//  \1 cc
                || dx1 <= 0 && dy1 <= 0 && dx2 <= 0 && dy2 >= 0//           \2 cc
                || dx1 <= 0 && dy1 <= 0 && dx2 < 0 && dy2 < 0 && m1 > m2//  \3 cc
*/
                || dx1 <= 0 && dy1 <= 0 && dx2 < 0 && dy2 < 0 && m1 < m2//  \3 c
                || dx1 <= 0 && dy1 <= 0 && dx2 >= 0 && dy2 <= 0//           \4 c

                || dx1 >= 0 && dy1 <= 0 && dx2 < 0 && dy2 > 0 && m1 > m2//  /2 c
/*
                || dx1 >= 0 && dy1 <= 0 && dx2 < 0 && dy2 > 0 && m1 < m2//  /2 cc
                || dx1 >= 0 && dy1 <= 0 && dx2 <= 0 && dy2 <= 0//           /3 cc
                || dx1 >= 0 && dy1 <= 0 && dx2 > 0 && dy2 < 0 && m1 > m2//  /4 cc
*/
                || dx1 >= 0 && dy1 <= 0 && dx2 > 0 && dy2 < 0 && m1 < m2//  /4 c
                || dx1 >= 0 && dy1 <= 0 && dx2 >= 0 && dy2 >= 0//           /1 c
                ) ? 1 : -1;
            }
            
            if (i > 1 && pdir !== dir)
            {
                count++;
                
                if (count === 2)
                {
                    p1.delete();
                    p2.delete();
                    plen -= 2;
                    i = count = 0;
                    indices.splice(indices.indexOf(p1),1);
                }
                else if (indices.indexOf(p2) === -1)
                {
                    indices.push(p2);
                }
            }
            else
            {
                if (pdir === dir)
                count = 0;
            }
            
            pdir = dir;
        }
        
        var p = points[plen-1];
        
        for (var i=indices.length-1; i>=0; i--)
        {
            var xp = indices[i];
            var object = this.object;
            var line = new Line(object);
            
            points = this.points;
            line.points = points.splice( points.indexOf( xp ) , points.length );
            xp.line = line;
            points[points.length-1].link( xp );
        }
        
        p.type = '';
        p.update();
    };
    
    this.update = function()
    {
        var points = this.points;
        var plen = points.length;
        var element = this.element;
        var canvas = Canvas.element;
        var p = points[0];
        var d = 'M';
            
        if (plen > 2)
        {
            d += p.cx+' '+p.cy+' C'+p.cx+' '+p.cy+' ';

            var p1,p2,
                dx1,dx2,dy1,dy2,
                m1,m2,mn1,mn2,
                x1,x2,x3,y1,y2,y3,
                hx1,hx2,hy1,hy2,
                ix1,ix2,iy1,iy2,
                b1,b2,bn1,bn2,
                lh1,lh2,li1,li2,
                theta1,theta2;

            for (var i=1; i<plen-1; i++)
            {
                p1 = points[i-1]; p2 = points[i]; p = points[i+1];
                p1.line = p2.line = p.line = undefined;
                p2.type = p2.type === 'link' ? 'curve' : '';
                x1 = p1.cx; x2 = p2.cx; x3 = p.cx;
                y1 = p1.cy; y2 = p2.cy; y3 = p.cy;
                dx1 = x2-x1; dx2 = x3-x2;
                dy1 = y2-y1; dy2 = y3-y2;
                m1 = dy1/dx1; m2 = dy2/dx2;
                hx1 = x2 - (dx1 / 2); hy1 = y2 - (dy1 / 2);
                hx2 = x3 - (dx2 / 2); hy2 = y3 - (dy2 / 2);
                mn1 = -1 / m1; mn2 = -1 / m2;
                b1 = hy1 - (m1 * hx1); b2 = hy2 - (m2 * hx2);
                bn1 = hy1 - (mn1 * hx1); bn2 = hy2 - (mn2 * hx2);
                iy1 = ((mn1*b2) - (m2*bn1)) / (mn1 - m2); ix1 = (iy1 - bn1) / mn1;
                iy2 = ((mn2*b1) - (m1*bn2)) / (mn2 - m1); ix2 = (iy2 - bn2) / mn2;
                lh1 = Math.sqrt(Math.pow(x2-hx1,2) + Math.pow(y2-hy1,2));
                lh2 = Math.sqrt(Math.pow(x2-hy2,2) + Math.pow(y2-hy2,2));
                li1 = Math.sqrt(Math.pow(hx1-ix1,2) + Math.pow(hy1-iy1,2));
                li2 = Math.sqrt(Math.pow(hx2-ix2,2) + Math.pow(hy2-iy2,2));
                theta1 = Math.atan(li1/lh1) / 2; theta2 = Math.atan(li2/lh2) / 2;
                
                d += p2.cx+' '+p2.cy+' '+p2.cx+' '+p2.cy+' C'+p2.cx+' '+p2.cy+' ';
            }

            d += p.cx+' '+p.cy+' ';
        }
        else if (plen > 1)
        {
            d += p.cx+' '+p.cy+' L';
            p = points[plen-1];
        }
      
        d += p.cx+' '+p.cy;
        element.setAttribute('d',d);
        canvas.insertBefore(element,canvas.children[0]);
    };
    
    this.delete = function()
    {
        var e = this.element;
        var object = this.object;
        var index = object.indexOf(this);
        
        e.parentNode.removeChild(e);
        object.lines.splice(index,1);
        CURRENT[3] = object.lines.length;
        
        if (!object.lines.length) {object.delete();}
    };
    
    this.translate = function(dx,dy,dz)
    {
        var points = this.points;

        for (var i=0; i<points.length; i++)
        {
            var point = points[i];
            
            if (point.anchor === null)
            {
                point.translate(dx,dy,dz);
            }
        }
    };
    
    this.init();
};

var Point = function(x,y,z)
{
    this.type = '';
    this.line;
    this.anchor = null;
    this.links = [];
    this.x = x;
    this.y = y;
    this.z = z;
    this.cx;
    this.cy;
    this.element = document.createElementNS(SVG_NS,'circle');
    this.data = [];
    
    this.init = function()
    {
        var element = this.element;
        
        element.setAttribute('r',5);
        element.setAttribute('name','point');
        element.setAttribute('class',this.type);
        this.update();
    };
    
    this.link = function(p)
    {
        p.type = 'link';
        p.anchor = this;
        this.type = 'anchor';
        this.links.push(p);
        this.update();
    };
    
    this.delete = function()
    {
        var line = this.line;
        var e = this.element;
        var index = line.points.indexOf(this);
        var links = this.links;
        var anchor = links[0];
        
        this.line = undefined;
        e.parentNode.removeChild(e);
        line.points.splice(index,1);
        
        if (anchor !== undefined)
        {
            if (links.length)
            {
                anchor.type = 'anchor';
                
                for (var i=1; i<links.length; i++)
                {
                    var link = links[i];
                    
                    link.anchor = anchor;
                    anchor.links.push(link);
                }
            }
            else
            {
                anchor.type = '';
            }
            
            anchor.update();
        }
        
        if (line.points.length < 2) {line.delete();}
    };
    
    this.update = function()
    {
        var element = this.element;
        var ox = ORIGIN.x;
        var oy = ORIGIN.y;
        var fl_average = (ox + oy) / 2;
        var x = this.x;
        var y = this.y;
        var px = x + ox;
        var py = y + oy;
        var pz = this.z;
        var cx = this.cx = !VIEW ? px : (px*fl_average)/(pz+fl_average);
        var cy = this.cy = !VIEW ? py : (py*fl_average)/(pz+fl_average);
        var links = this.links;
        var line = this.line;
        var canvas = Canvas.element;
        var type = this.type;
        var child = type === 'curve' ? canvas.children[0] : null;
        
        for (var i=0; i<links.length; i++)
        {
            var link = links[i];
            
            link.x = x;
            link.y = y;
            link.z = pz;
            link.update();
        }
        
        if (line !== undefined)
        {
            line.update();
            this.data[4] = line.points.indexOf(this);
        }
        
        canvas.insertBefore(element,child);
        element.setAttribute('cx',cx);
        element.setAttribute('cy',cy);
        element.setAttribute('class',type);
        element.setAttribute('data',this.data.join());
        
        return {x:cx,y:cy};
    };
    
    this.translate = function(dx,dy,dz)
    {
        this.x += dx;
        this.y += dy;
        this.z += dz;
        this.update();
    };
    
    this.rotateX = function(delta,origin)
    {
        var o = origin === undefined ? ORIGIN : origin;
        
        this.rotate(o.z,o.y,this.z,this.y,delta);
    };
    
    this.rotateY = function(delta,origin)
    {
        var o = origin === undefined ? ORIGIN : origin;
        
        this.rotate(o.x,o.z,this.x,this.z,delta);
    };
    
    this.rotateZ = function(delta,origin)
    {
        var o = origin === undefined ? ORIGIN : origin;
        
        this.rotate(o.x,o.y,this.x,this.y,delta);
    };
    
    this.rotate = function(oh,ov,ph,pv,d)
    {
        this.x = (Math.cos(d)*(ph-oh)) - (Math.sin(d)*(pv-ov)) + oh;
        this.y = (Math.cos(d)*(pv-ov)) + (Math.sin(d)*(ph-oh)) + ov;
        this.update();
    };
    
    this.clone = function()
    {
        var p = new Point(this.x,this.y,this.z);
        var d = this.data;
        
        for (var i=0; i<d.length; i++)
        {
            p.data[i] = d[i];
        }
        
        return p;
    };
    
    this.init();
};

Nav.init();
Canvas.init();
        </script>
    </body>
</html>